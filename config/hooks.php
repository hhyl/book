<?php

/**
 * 自定义钩子
 *
 */

/*
\Phpcmf\Hooks::on('cms_init', function() {
    // cms 初始化后的运行
});*/


\Phpcmf\Hooks::on('pay_success', function($data) {
    // cms 分析订单来源
    list($a, $b, $c, $d) = explode('-', $data['mid']);
    list($sid, $mid) = explode('_', $a);
    if ($sid && $mid == 'book') {
        // 更新记录表
        \Phpcmf\Service::M()->db->table($sid.'_'.$mid.'_buy')->insert(array(
            'cid' => $b,
            'uid' => $data['uid'],
        ));
        // 打款到收款人账户
        \Phpcmf\Service::M('pay')->add_money($data['touid'], abs($data['value']));
        // 收款人增加一条收入记录
        \Phpcmf\Service::M('pay')->add_paylog([
            'uid' => $data['touid'],
            'username' => $data['tousername'],
            'touid' => $data['uid'],
            'tousername' => $data['username'],
            'mid' => $data['mid'],
            'title' => $data['title'],
            'value' => abs($data['value']),
            'type' => $data['type'],
            'status' => 1,
            'result' => $payid,
            'paytime' => SYS_TIME,
            'inputtime' => $data['inputtime'],
        ]);
    }
});