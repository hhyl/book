<?php namespace My\Field;

class Stype extends \Phpcmf\Library\A_Field {
	
	/**
     * 构造函数
     */
    public function __construct(...$params) {
        parent::__construct(...$params);
		$this->fieldtype = ['TEXT' => '']; // TRUE表全部可用字段类型,自定义格式为 array('可用字段类型名称' => '默认长度', ... )
		$this->defaulttype = 'TEXT'; // 当用户没有选择字段类型时的缺省值
    }
	
	/**
	 * 字段相关属性参数
	 *
	 * @param	array	$value	值
	 * @return  string
	 */
	public function option($option) {

		return [''];
	}

    /**
     * 创建sql语句
     */
    public function create_sql($name, $value, $cname) {
        $sql = 'ALTER TABLE `{tablename}` ADD `'.$name.'` INT(5) UNSIGNED NULL COMMENT \''.$cname.'\'';
        return $sql;
    }

    /**
     * 修改sql语句
     */
    public function alter_sql($name, $value, $cname) {
        return NULL;
    }

    /**
     * 删除sql语句
     */
    public function drop_sql($name) {
        return 'ALTER TABLE `{tablename}` DROP `'.$name.'`';
    }
	
	/**
	 * 字段输出
	 */
	public function output($value) {
		return $value;
	}
	
	/**
	 * 字段入库值
	 */
	public function insert_value($field) {

        \Phpcmf\Service::L('field')->data[$field['ismain']][$field['fieldname']] = intval(\Phpcmf\Service::L('field')->post[$field['fieldname']]);

	}

    /**
     * 字段表单输入
     *
     * @return  string
     */
    public function input($field, $value = '') {

        // 字段禁止修改时就返回显示字符串
        if ($this->_not_edit($field, $value)) {
            return $this->show($field, $value);
        }

        // 字段存储名称
        $name = $field['fieldname'];

        // 字段显示名称
        $text = ($field['setting']['validate']['required'] ? '<span class="required" aria-required="true"> * </span>' : '').$field['name'];

        // 表单附加参数
        $attr = $field['setting']['validate']['formattr'];

        // 字段提示信息
        $tips = ($name == 'title' && APP_DIR) || $field['setting']['validate']['tips'] ? '<span class="help-block" id="dr_'.$field['fieldname'].'_tips">'.$field['setting']['validate']['tips'].'</span>' : '';

        // 是否必填
        $required =  $field['setting']['validate']['required'] ? ' required="required"' : '';


        $str = '<label><select '.$required.' class="form-control '.$field['setting']['option']['css'].'" name="data['.$name.']" id="dr_'.$name.'" '.$attr.' >';

        // 字段默认值
        $value = isset($_GET['tid']) ? intval($_GET['tid']) : $value;

        // 表单选项
        $options = dr_son_type(\Phpcmf\Service::C()->index['son_type']);
        if ($options) {
            foreach ($options as $v => $n) {
                $str.= '<option value="'.$v.'" '.($v == $value ? ' selected' : '').'>'.$n.'</option>';
            }
        }

        $str.= '</select></label>'.$tips;
        return $this->input_format($name, $text, $str);
    }



    /**
     * 字段表单显示
     *
     * @param	string	$field	字段数组
     * @param	array	$value	值
     * @return  string
     */
    public function show($field, $value = null) {

        $options = dr_format_option_array($field['setting']['option']['options']);

        $str = '<div class="form-control-static"> '.(isset($options[$value]) ? $options[$value] : dr_lang('未选择')).' </div>';

        return $this->input_format($field['fieldname'], $field['name'], $str);
    }
}