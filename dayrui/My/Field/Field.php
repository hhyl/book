<?php

/**
 * 系统可用字段
 * id 文件名称
 * name 显示名称
 * used 数组 表示可以用到哪些地方
 * namespace 是哪个app专属的
 */

return [
    [
        'id' => 'Stype',
        'name' => '章节分类',
        'used' => '',
        'namespace' => '',
    ],
];