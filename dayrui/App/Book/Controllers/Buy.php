<?php namespace Phpcmf\Controllers;

class Buy extends \Phpcmf\Common {

    public function index() {

        !$this->uid && exit($this->_msg(0, dr_lang('登录之后才能购买')));

        $id = (int)\Phpcmf\Service::L('Input')->get('id');
        $fid = (int)\Phpcmf\Service::L('Input')->get('fid');
        (!$fid || !$id) && exit($this->_msg(0, dr_lang('支付参数不完整')));

        $num = max(1, (int)\Phpcmf\Service::L('Input')->get('num'));
        $sku = dr_safe_replace(\Phpcmf\Service::L('Input')->get('sku'), 'undefined');

        $field = $this->get_cache('table-field', $fid);
        !$field && exit($this->_msg(0, dr_lang('支付字段不存在')));

        if (\Phpcmf\Service::M()->db->table(SITE_ID.'_'.APP_DIR.'_buy')
            ->where('uid', $this->uid)->where('cid', $id)->countAllResults()) {
            exit($this->_msg(0, dr_lang('已经购买过了')));
        }

        // 获取付款价格
        $rt = \Phpcmf\Service::M('pay')->get_pay_info($id, $field, $num, $sku);
        isset($rt['code']) && !$rt['code'] && exit($this->_msg(0, $rt['msg']));

        \Phpcmf\Service::V()->assign($rt);
        \Phpcmf\Service::V()->assign([
            'num' => $rt['num'],
            'price' => $rt['price'],
            'total' => $rt['total'],
            'payform' => dr_payform($rt['mid'], $rt['total'], $rt['title'].$rt['sku_string'], $rt['url']),
            'meta_title' => dr_lang('在线付款').SITE_SEOJOIN.SITE_NAME,
            'meta_keywords' => $this->get_cache('site', SITE_ID, 'config', 'SITE_KEYWORDS'),
            'meta_description' => $this->get_cache('site', SITE_ID, 'config', 'SITE_DESCRIPTION')
        ]);
        \Phpcmf\Service::V()->display('buy.html');
    }
}
