<?php namespace Phpcmf\Controllers\Member;

// 购买的
class Buy extends \Phpcmf\Table
{

    /**
     * 记录
     */
    public function index() {

        $table = dr_module_table_prefix(APP_DIR);

        $this->_init([
            'table' => $table,
            'order_by' => 'inputtime desc',
            'date_field' => 'inputtime',
            'where_list' => '`id` IN (select cid from `'.\Phpcmf\Service::M()->dbprefix($table).'_buy` where `uid`='.$this->uid.')',
        ]);

        $this->_List();

        \Phpcmf\Service::V()->display('buy.html');
    }

}
