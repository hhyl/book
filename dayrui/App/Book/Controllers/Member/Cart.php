<?php namespace Phpcmf\Controllers\Member;

// 书架
class Cart extends \Phpcmf\Table
{

    /**
     * 书架
     */
    public function index() {

        $table = dr_module_table_prefix(APP_DIR);

        $this->_init([
            'table' => $table.'_cart',
            #'select_list' => $table.'_cart.*,'.$table.'.title,'.$table.'.url',
            'order_by' => $table.'_cart.id desc',
            'join_list' => [$table, $table.'.id='.$table.'_cart.cid', 'left'],
            'where_list' => $table.'_cart.uid='.$this->uid.'',
        ]);

        $this->_List();

        \Phpcmf\Service::V()->display('cart.html');
    }

    /**
     * 删除
     */
    public function delete() {

        !IS_POST && $this->_json(0, dr_lang('请求错误'));

        $ids = \Phpcmf\Service::L('Input')->get_post_ids();
        !$ids && $this->_json(0, dr_lang('所选数据不存在'));

        // 格式化
        $in = [];
        foreach ($ids as $i) {
            $i && $in[] = intval($i);
        }

        !$in && $this->_json(0, dr_lang('所选数据不存在'));

        \Phpcmf\Service::M()->db->table(dr_module_table_prefix(APP_DIR).'_cart')->where('uid', $this->uid)->whereIn('cid', $in)->delete();

        $this->_json(1, dr_lang('操作成功'));
    }


}
