<?php namespace Phpcmf\Controllers;

class Son extends \Phpcmf\Home\Mform
{

    public function index() {
        $this->_Home_List();
    }

    public function show() {
        $this->_Home_Show();
    }

    protected function _Call_Show($data) {


        $_test_0 = 0;
        $_test_1 = [];
        $data['prev_page'] = $data['next_page'] = [];

        $son = \Phpcmf\Service::M()->table(SITE_ID.'_'.MOD_DIR.'_form_son')
            ->where('cid', $data['cid'])
            ->where('status=1')
            ->order_by('displayorder asc,id asc')->getAll();
        if ($son) {
            foreach ($son as $i => $t) {
                $son[$i]['url'] = dr_son_url($t);
                if ($t['id'] == $data['id']) {
                    // 找到了
                    $_test_0 = 1;
                    $data['prev_page'] = $_test_1;
                } elseif ($_test_0) {
                    $_test_0 = 0;
                    $data['next_page'] = $son[$i];
                }
                $_test_1 = $son[$i];
            }
        }

        $data['son_list'] = dr_format_son_list($son, $this->index['son_type']);
        $data['son_type'] = dr_son_type($this->index['son_type']);


        return $data;
    }
}
