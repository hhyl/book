<?php namespace Phpcmf\Controllers;

class Show extends \Phpcmf\Home\Module
{

    public function index() {
        $this->_module_init();
        $this->_Show(
            (int)\Phpcmf\Service::L('Input')->get('id'),
            [
                'field' => dr_safe_replace(\Phpcmf\Service::L('Input')->get('field')),
                'value' => dr_safe_replace(\Phpcmf\Service::L('Input')->get('value')),
            ],
            max(1, (int)\Phpcmf\Service::L('Input')->get('page'))
        );
    }

    protected function _Call_Show($data) {

        $url = '';
        $son = \Phpcmf\Service::M()->table(SITE_ID.'_'.MOD_DIR.'_form_son')
            ->where('cid', $data['id'])
            ->where('status=1')
            ->order_by('displayorder asc,id asc')->getAll();
        if ($son) {
            foreach ($son as $i => $t) {
                $son[$i]['url'] = dr_son_url($t);
                !$url && $url = $son[$i]['url'];
            }
        }

        $data['son_url'] = $url;
        $data['son_list'] = dr_format_son_list($son, $data['son_type']);
        $data['son_type'] = dr_son_type($data['son_type']);

        return $data;
    }

    public function time() {
        $this->_module_init();
        $this->_MyShow(
            'time',
            (int)\Phpcmf\Service::L('Input')->get('id'),
            max(1, (int)\Phpcmf\Service::L('Input')->get('page'))
        );
    }

    public function recycle() {
        $this->_module_init();
        $this->_MyShow(
            'recycle',
            (int)\Phpcmf\Service::L('Input')->get('id'),
            max(1, (int)\Phpcmf\Service::L('Input')->get('page'))
        );
    }

    public function draft() {
        $this->_module_init();
        $this->_MyShow(
            'draft',
            (int)\Phpcmf\Service::L('Input')->get('id'),
            max(1, (int)\Phpcmf\Service::L('Input')->get('page'))
        );
    }

    public function verify() {
        $this->_module_init();
        $this->_MyShow(
            'verify',
            (int)\Phpcmf\Service::L('Input')->get('id'),
            max(1, (int)\Phpcmf\Service::L('Input')->get('page'))
        );
    }

    /**
     * 加入书架
     */
    public function cart() {

        !$this->uid && $this->_json(0, dr_lang('还没有登录'));

        $id = (int)\Phpcmf\Service::L('Input')->get('id');
        !$id && $this->_json(0, dr_lang('id参数不完整'));

        $this->tablename = SITE_ID.'_'.APP_DIR;
        $data = \Phpcmf\Service::M()->db->table($this->tablename.'_index')->where('id', $id)->countAllResults();
        !$data && $this->_json(0, dr_lang('模块内容不存在'));

        $cart = \Phpcmf\Service::M()->db->table($this->tablename.'_cart')->where('cid', $id)->where('uid', $this->uid)->get()->getRowArray();
        if ($cart) {
            // 已经收藏了,我们就删除它
            \Phpcmf\Service::M()->db->table($this->tablename.'_cart')->where('id', intval($cart['id']))->delete();
            $msg = dr_lang('移除书架');
            $c = 0;
        } else {
            \Phpcmf\Service::M()->db->table($this->tablename.'_cart')->insert(array(
                'cid' => $id,
                'uid' => $this->uid,
                'read_name' => '',
                'read_url' => '',
            ));
            $msg = dr_lang('加入书架');
            $c = 1;
        }
        // 返回结果
        $this->_json(1, $msg, $c);
    }

    public function add_cart() {

        !$this->uid && $this->_json(0, dr_lang('还没有登录'));

        $id = (int)\Phpcmf\Service::L('Input')->get('id');
        !$id && $this->_json(0, dr_lang('id参数不完整'));

        $sid = (int)\Phpcmf\Service::L('Input')->get('sid');
        !$sid && $this->_json(0, dr_lang('sid参数不完整'));

        $this->tablename = SITE_ID.'_'.APP_DIR;
        $data = \Phpcmf\Service::M()->table($this->tablename.'_form_son')->where('id', $sid)->getRow();
        !$data && $this->_json(0, dr_lang('章节内容不存在'));

        $cart = \Phpcmf\Service::M()->db->table($this->tablename.'_cart')->where('cid', $id)->where('uid', $this->uid)->get()->getRowArray();
        if ($cart) {
            \Phpcmf\Service::M()->table($this->tablename.'_cart')->update($cart['id'], array(
                'read_name' => $data['title'],
                'read_url' => dr_son_url($data),
            ));
            $this->_json(1, 'ok');
        }

        $this->_json(0, dr_lang('未加入书架'));
    }
}
