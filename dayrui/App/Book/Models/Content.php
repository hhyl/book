<?php namespace Phpcmf\Model\Book;

// 模块内容模型类

class Content extends \Phpcmf\Model\Content {


    // 内容显示
    public function _call_show($data) {

        $url = '';
        $son = \Phpcmf\Service::M()->table(SITE_ID.'_'.MOD_DIR.'_form_son')
            ->where('cid', $data['id'])
            ->where('status=1')
            ->order_by('displayorder asc,id asc')->getAll();
        if ($son) {
            foreach ($son as $i => $t) {
                $son[$i]['url'] = dr_son_url($t);
                !$url && $url = $son[$i]['url'];
            }
        }

        $data['son_url'] = $url;
        $data['son_list'] = dr_format_son_list($son, $data['son_type']);
        $data['son_type'] = dr_son_type($data['son_type']);


        return $data;
    }
}