DROP TABLE IF EXISTS `{tablename}_cart`;
CREATE TABLE IF NOT EXISTS `{tablename}_cart` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cid` int(10) UNSIGNED NOT NULL COMMENT '文档id',
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT 'uid',
  `read_name` VARCHAR (255) NOT NULL COMMENT '阅读',
  `read_url` VARCHAR (255) NOT NULL COMMENT '阅读',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='书架表';

DROP TABLE IF EXISTS `{tablename}_buy`;
CREATE TABLE IF NOT EXISTS `{tablename}_buy` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'id',
  `cid` int(10) UNSIGNED NOT NULL COMMENT '文档id',
  `uid` mediumint(8) UNSIGNED NOT NULL COMMENT 'uid',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='购买记录表';