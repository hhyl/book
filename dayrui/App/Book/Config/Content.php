<?php

return array (
    'table' =>
        array (
            1 => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` smallint(5) unsigned NOT NULL COMMENT \'栏目id\',
  `title` varchar(255) DEFAULT NULL COMMENT \'主题\',
  `thumb` varchar(255) DEFAULT NULL COMMENT \'缩略图\',
  `keywords` varchar(255) DEFAULT NULL COMMENT \'关键字\',
  `description` text COMMENT \'描述\',
  `hits` int(10) unsigned DEFAULT NULL COMMENT \'浏览数\',
  `uid` int(10) unsigned NOT NULL COMMENT \'作者id\',
  `author` varchar(50) NOT NULL COMMENT \'作者名称\',
  `status` tinyint(2) NOT NULL COMMENT \'状态\',
  `url` varchar(255) DEFAULT NULL COMMENT \'地址\',
  `link_id` int(10) NOT NULL DEFAULT \'0\' COMMENT \'同步id\',
  `tableid` smallint(5) unsigned NOT NULL COMMENT \'附表id\',
  `inputip` varchar(15) DEFAULT NULL COMMENT \'录入者ip\',
  `inputtime` int(10) unsigned NOT NULL COMMENT \'录入时间\',
  `updatetime` int(10) unsigned NOT NULL COMMENT \'更新时间\',
  `comments` int(10) unsigned DEFAULT \'0\' COMMENT \'评论数量\',
  `favorites` int(10) unsigned DEFAULT \'0\' COMMENT \'收藏数量\',
  `avgsort` decimal(10,2) unsigned DEFAULT \'0.00\' COMMENT \'平均点评分数\',
  `support` int(10) unsigned DEFAULT \'0\' COMMENT \'支持数\',
  `oppose` int(10) unsigned DEFAULT \'0\' COMMENT \'反对数\',
  `donation` decimal(10,2) unsigned DEFAULT \'0.00\' COMMENT \'捐赠总额\',
  `displayorder` int(10) DEFAULT \'0\' COMMENT \'排序值\',
  `biming` varchar(255) DEFAULT NULL COMMENT \'笔名\',
  `son_total` int(10) unsigned DEFAULT \'0\' COMMENT \'表单章节统计\',
  `son_type` text COMMENT \'分卷类别\',
  `zt` tinyint(1) DEFAULT \'0\' COMMENT \'连载状态\',
  `price` decimal(10,2) DEFAULT NULL COMMENT \'售价\',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`),
  KEY `catid` (`catid`,`updatetime`),
  KEY `link_id` (`link_id`),
  KEY `comments` (`comments`),
  KEY `avgsort` (`avgsort`),
  KEY `support` (`support`),
  KEY `oppose` (`oppose`),
  KEY `donation` (`donation`),
  KEY `favorites` (`favorites`),
  KEY `status` (`status`),
  KEY `updatetime` (`updatetime`),
  KEY `hits` (`hits`),
  KEY `displayorder` (`displayorder`,`updatetime`),
  KEY `son_total` (`son_total`)
) ENGINE=MyISAM AUTO_INCREMENT=1025 DEFAULT CHARSET=utf8 COMMENT=\'内容主表\'',
            0 => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL,
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者uid\',
  `catid` smallint(5) unsigned NOT NULL COMMENT \'栏目id\',
  `content` mediumtext COMMENT \'内容\',
  UNIQUE KEY `id` (`id`),
  KEY `uid` (`uid`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT=\'内容附表\'',
        ),
    'field' =>
        array (
            1 =>
                array (
                    0 =>
                        array (
                            'name' => '书名',
                            'fieldname' => 'title',
                            'fieldtype' => 'Text',
                            'isedit' => '1',
                            'ismain' => '1',
                            'issystem' => '1',
                            'ismember' => '1',
                            'issearch' => '0',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'fieldtype' => 'VARCHAR',
                                            'fieldlength' => '255',
                                            'value' => '',
                                            'width' => '300',
                                            'unique' => '1',
                                            'css' => '',
                                        ),
                                    'validate' =>
                                        array (
                                            'required' => '1',
                                            'pattern' => '',
                                            'errortips' => '',
                                            'xss' => '1',
                                            'check' => '',
                                            'filter' => '',
                                            'formattr' => 'onblur="check_title();get_keywords(\'keywords\');"',
                                            'tips' => '',
                                        ),
                                    'is_right' => '0',
                                ),
                            'displayorder' => '0',
                        ),
                    1 =>
                        array (
                            'name' => '封面图',
                            'fieldname' => 'thumb',
                            'fieldtype' => 'File',
                            'isedit' => '1',
                            'ismain' => '1',
                            'issystem' => '1',
                            'ismember' => '1',
                            'issearch' => '0',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'input' => '1',
                                            'ext' => 'jpg,gif,png',
                                            'size' => '10',
                                            'attachment' => '0',
                                            'css' => '',
                                        ),
                                    'validate' =>
                                        array (
                                            'required' => '0',
                                            'pattern' => '',
                                            'errortips' => '',
                                            'check' => '',
                                            'filter' => '',
                                            'formattr' => '',
                                            'tips' => '',
                                        ),
                                    'is_right' => '0',
                                ),
                            'displayorder' => '0',
                        ),
                    2 =>
                        array (
                            'name' => '关键字',
                            'fieldname' => 'keywords',
                            'fieldtype' => 'Text',
                            'isedit' => '1',
                            'ismain' => '1',
                            'issystem' => '1',
                            'ismember' => '1',
                            'issearch' => '1',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'width' => 400,
                                            'fieldtype' => 'VARCHAR',
                                            'fieldlength' => '255',
                                        ),
                                    'validate' =>
                                        array (
                                            'xss' => 1,
                                            'formattr' => ' data-role="tagsinput"',
                                        ),
                                ),
                            'displayorder' => '0',
                        ),
                    3 =>
                        array (
                            'name' => '笔名',
                            'fieldname' => 'biming',
                            'fieldtype' => 'Text',
                            'isedit' => '1',
                            'ismain' => '1',
                            'issystem' => '0',
                            'ismember' => '1',
                            'issearch' => '0',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'fieldtype' => '',
                                            'fieldlength' => '',
                                            'value' => '',
                                            'width' => '',
                                            'css' => '',
                                        ),
                                    'validate' =>
                                        array (
                                            'required' => '0',
                                            'pattern' => '',
                                            'errortips' => '',
                                            'check' => '',
                                            'filter' => '',
                                            'formattr' => '',
                                            'tips' => '',
                                        ),
                                    'is_right' => '0',
                                ),
                            'displayorder' => '0',
                        ),
                    4 =>
                        array (
                            'name' => '连载状态',
                            'fieldname' => 'zt',
                            'fieldtype' => 'Radio',
                            'isedit' => '1',
                            'ismain' => '1',
                            'issystem' => '0',
                            'ismember' => '1',
                            'issearch' => '0',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'options' => '连载中|0
已完结|1',
                                            'value' => '0',
                                            'fieldtype' => 'TINYINT',
                                            'fieldlength' => '1',
                                            'show_type' => '0',
                                            'css' => '',
                                        ),
                                    'validate' =>
                                        array (
                                            'required' => '0',
                                            'pattern' => '',
                                            'errortips' => '',
                                            'check' => '',
                                            'filter' => '',
                                            'formattr' => '',
                                            'tips' => '',
                                        ),
                                    'is_right' => '0',
                                ),
                            'displayorder' => '0',
                        ),
                    5 =>
                        array (
                            'name' => 'VIP费用',
                            'fieldname' => 'price',
                            'fieldtype' => 'Pay',
                            'isedit' => '1',
                            'ismain' => '1',
                            'issystem' => '0',
                            'ismember' => '1',
                            'issearch' => '0',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'payfile' => 'buy.html',
                                            'is_finecms' => '1',
                                            'width' => 'book.html',
                                            'css' => '',
                                        ),
                                    'validate' =>
                                        array (
                                            'required' => '0',
                                            'pattern' => '',
                                            'errortips' => '',
                                            'check' => '',
                                            'filter' => '',
                                            'formattr' => '',
                                            'tips' => '',
                                        ),
                                    'is_right' => '0',
                                ),
                            'displayorder' => '0',
                        ),
                    6 =>
                        array (
                            'name' => '简要描述',
                            'fieldname' => 'description',
                            'fieldtype' => 'Textarea',
                            'isedit' => '1',
                            'ismain' => '1',
                            'issystem' => '1',
                            'ismember' => '1',
                            'issearch' => '0',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'value' => '',
                                            'width' => '500',
                                            'height' => '60',
                                            'css' => '',
                                        ),
                                    'validate' =>
                                        array (
                                            'required' => '0',
                                            'pattern' => '',
                                            'errortips' => '',
                                            'xss' => '1',
                                            'check' => '',
                                            'filter' => 'dr_clearhtml',
                                            'formattr' => '',
                                            'tips' => '',
                                        ),
                                    'is_right' => '0',
                                ),
                            'displayorder' => '9',
                        ),
                ),
            0 =>
                array (
                    0 =>
                        array (
                            'name' => '内容简介',
                            'fieldname' => 'content',
                            'fieldtype' => 'Ueditor',
                            'isedit' => '1',
                            'ismain' => '0',
                            'issystem' => '1',
                            'ismember' => '1',
                            'issearch' => '0',
                            'disabled' => '0',
                            'setting' =>
                                array (
                                    'option' =>
                                        array (
                                            'watermark' => '0',
                                            'show_bottom_boot' => '0',
                                            'mini' => '0',
                                            'mobile_mini' => '0',
                                            'autofloat' => '0',
                                            'autoheight' => '0',
                                            'page' => '0',
                                            'mode' => '1',
                                            'tool' => '\'bold\', \'italic\', \'underline\'',
                                            'mode2' => '1',
                                            'tool2' => '\'bold\', \'italic\', \'underline\'',
                                            'mode3' => '1',
                                            'tool3' => '\'bold\', \'italic\', \'underline\'',
                                            'attachment' => '0',
                                            'value' => '',
                                            'width' => '100%',
                                            'height' => '400',
                                            'css' => '',
                                        ),
                                    'validate' =>
                                        array (
                                            'required' => '1',
                                            'pattern' => '',
                                            'errortips' => '',
                                            'xss' => '1',
                                            'check' => '',
                                            'filter' => '',
                                            'formattr' => '',
                                            'tips' => '',
                                        ),
                                    'is_right' => '0',
                                ),
                            'displayorder' => '9',
                        ),
                ),
        ),
);