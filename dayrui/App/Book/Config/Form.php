<?php

return array (
    'son' =>
        array (
            'form' =>
                array (
                    'id' => '1',
                    'name' => '章节',
                    'table' => 'son',
                    'module' => 'book',
                    'disabled' => '0',
                    'setting' => '',
                ),
            'table' =>
                array (
                    0 => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL,
  `cid` int(10) unsigned NOT NULL COMMENT \'内容id\',
  `catid` mediumint(8) unsigned NOT NULL COMMENT \'栏目id\',
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者id\',
  `neirong` mediumtext COMMENT \'章节内容\',
  UNIQUE KEY `id` (`id`),
  KEY `cid` (`cid`),
  KEY `catid` (`catid`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT=\'模块表单章节附表\'',
                    1 => 'CREATE TABLE IF NOT EXISTS `{tablename}` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cid` int(10) unsigned NOT NULL COMMENT \'内容id\',
  `catid` mediumint(8) unsigned NOT NULL COMMENT \'栏目id\',
  `uid` mediumint(8) unsigned NOT NULL COMMENT \'作者id\',
  `author` varchar(50) NOT NULL COMMENT \'作者名称\',
  `inputip` varchar(30) DEFAULT NULL COMMENT \'录入者ip\',
  `inputtime` int(10) unsigned NOT NULL COMMENT \'录入时间\',
  `title` varchar(255) DEFAULT NULL COMMENT \'表单主题\',
  `status` tinyint(1) DEFAULT NULL COMMENT \'状态值\',
  `tableid` smallint(5) unsigned NOT NULL COMMENT \'附表id\',
  `displayorder` int(10) DEFAULT NULL COMMENT \'排序值\',
  `tid` int(5) unsigned DEFAULT NULL COMMENT \'分卷分类\',
  `vip` tinyint(1) DEFAULT \'0\' COMMENT \'是否收费\',
  PRIMARY KEY (`id`),
  KEY `cid` (`cid`),
  KEY `uid` (`uid`),
  KEY `catid` (`catid`),
  KEY `author` (`author`),
  KEY `status` (`status`),
  KEY `displayorder` (`displayorder`),
  KEY `inputtime` (`inputtime`)
) ENGINE=MyISAM AUTO_INCREMENT=10244 DEFAULT CHARSET=utf8 COMMENT=\'模块表单章节表\'',
                ),
            'field' =>
                array (
                    0 =>
                        array (
                            0 =>
                                array (
                                    'name' => '章节内容',
                                    'fieldname' => 'neirong',
                                    'fieldtype' => 'Ueditor',
                                    'isedit' => '1',
                                    'ismain' => '0',
                                    'issystem' => '0',
                                    'ismember' => '1',
                                    'issearch' => '0',
                                    'disabled' => '0',
                                    'setting' =>
                                        array (
                                            'option' =>
                                                array (
                                                    'watermark' => '0',
                                                    'show_bottom_boot' => '0',
                                                    'mini' => '0',
                                                    'mobile_mini' => '0',
                                                    'autofloat' => '0',
                                                    'autoheight' => '0',
                                                    'page' => '0',
                                                    'mode' => '1',
                                                    'tool' => '\'bold\', \'italic\', \'underline\'',
                                                    'mode2' => '1',
                                                    'tool2' => '\'bold\', \'italic\', \'underline\'',
                                                    'mode3' => '1',
                                                    'tool3' => '\'bold\', \'italic\', \'underline\'',
                                                    'attachment' => '0',
                                                    'value' => '',
                                                    'width' => '100%',
                                                    'height' => '300',
                                                    'css' => '',
                                                ),
                                            'validate' =>
                                                array (
                                                    'required' => '0',
                                                    'pattern' => '',
                                                    'errortips' => '',
                                                    'check' => '',
                                                    'filter' => '',
                                                    'formattr' => '',
                                                    'tips' => '',
                                                    'xss' => 1,
                                                ),
                                            'is_right' => '0',
                                        ),
                                    'displayorder' => '0',
                                ),
                        ),
                    1 =>
                        array (
                            0 =>
                                array (
                                    'name' => '分卷',
                                    'fieldname' => 'tid',
                                    'fieldtype' => 'Stype',
                                    'isedit' => '1',
                                    'ismain' => '1',
                                    'issystem' => '0',
                                    'ismember' => '1',
                                    'issearch' => '0',
                                    'disabled' => '0',
                                    'setting' =>
                                        array (
                                            'validate' =>
                                                array (
                                                    'required' => '0',
                                                    'pattern' => '',
                                                    'errortips' => '',
                                                    'check' => '',
                                                    'filter' => '',
                                                    'formattr' => '',
                                                    'tips' => '',
                                                ),
                                            'option' =>
                                                array (
                                                    'css' => '',
                                                ),
                                            'is_right' => '0',
                                        ),
                                    'displayorder' => '-1',
                                ),
                            1 =>
                                array (
                                    'name' => '章节标题',
                                    'fieldname' => 'title',
                                    'fieldtype' => 'Text',
                                    'isedit' => '1',
                                    'ismain' => '1',
                                    'issystem' => '1',
                                    'ismember' => '1',
                                    'issearch' => '0',
                                    'disabled' => '0',
                                    'setting' =>
                                        array (
                                            'option' =>
                                                array (
                                                    'fieldtype' => 'VARCHAR',
                                                    'fieldlength' => '255',
                                                    'value' => '',
                                                    'width' => '400',
                                                    'css' => '',
                                                ),
                                            'validate' =>
                                                array (
                                                    'required' => '1',
                                                    'pattern' => '',
                                                    'errortips' => '',
                                                    'xss' => '1',
                                                    'check' => '',
                                                    'filter' => '',
                                                    'formattr' => '',
                                                    'tips' => '',
                                                ),
                                            'is_right' => '0',
                                        ),
                                    'displayorder' => '0',
                                ),
                            2 =>
                                array (
                                    'name' => '是否收费',
                                    'fieldname' => 'vip',
                                    'fieldtype' => 'Radio',
                                    'isedit' => '1',
                                    'ismain' => '1',
                                    'issystem' => '0',
                                    'ismember' => '0',
                                    'issearch' => '0',
                                    'disabled' => '0',
                                    'setting' =>
                                        array (
                                            'option' =>
                                                array (
                                                    'options' => '免费|0
收费|1',
                                                    'value' => '0',
                                                    'fieldtype' => 'TINYINT',
                                                    'fieldlength' => '1',
                                                    'show_type' => '0',
                                                    'css' => '',
                                                ),
                                            'validate' =>
                                                array (
                                                    'required' => '0',
                                                    'pattern' => '',
                                                    'errortips' => '',
                                                    'check' => '',
                                                    'filter' => '',
                                                    'formattr' => '',
                                                    'tips' => '',
                                                ),
                                            'is_right' => '0',
                                        ),
                                    'displayorder' => '0',
                                ),
                        ),
                ),
        ),
);